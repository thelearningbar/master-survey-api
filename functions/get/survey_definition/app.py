import pymysql
import pymysql.cursors
import os
import json
 
#retrieves a list of summarized survey definitions from the database
def lambda_handler(event, context):
    host = os.environ.get('DB_HOST')
    connection = None

    #checks if connection was successful
    try:
        connection = connect(host)
    except Exception as e:

        msg = "ERROR: Failed to connect to database."
        error = str(e)
        response = errorResponse(msg, error,400)
        
        return response
    
    #checks if theres any SQL query errors
    try:
        
        #A simple mySQL query that retrieves the id, title and description of all survey definitions.
        with connection.cursor() as cursor:
            query = "SELECT id, title, description FROM SurveyDefinition;"
            cursor.execute(query)
            records = cursor.fetchall()

    except Exception as e:

        msg = "Invalid or incorrect mySQL query"
        error = str(e)
        response = errorResponse(msg, error, 400)

        return response

    finally:
        connection.close()

    #returns pages in JSON data format
    body = {"SurveyDefinition": records}
    bodyToJson = json.dumps(body, indent = 4, default=str)
    response = createResponse(bodyToJson, 200)

    return response
    

#creates a connection with the database. 
def connect(host):

    connection = pymysql.connect(host = host, db = os.environ.get('DB_NAME'),
    user = os.environ.get('DB_USERNAME'), password = os.environ.get('DB_PASSWORD'),  
    cursorclass=pymysql.cursors.DictCursor)

    return connection


#creates the response JSON body
def createResponse(body, status):
    response = {
    "statusCode":status, 
    "headers":{"Access-Control-Allow-Origin": os.environ.get('ORIGIN')},
    "body":body
    }

    return response

#creates the error response JSON body
def errorResponse(msg,error,status):
    obj = {"message": msg, "error": error}
    objToJson = json.dumps(obj,indent=4,default=str)
    response = createResponse(objToJson, status)

    return response


