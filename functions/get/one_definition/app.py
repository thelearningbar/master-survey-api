import pymysql
import pymysql.cursors
import os
import json
 
#retrieves one survey definition from the database
def lambda_handler(event, context):
    host = os.environ.get('DB_HOST')
    connection = None

    #checks if connection was successful
    try:
        connection = connect(host)
    except Exception as e:
        msg = "ERROR: Failed to connect to database."
        error = str(e)
        response = errorResponse(msg, error,400)
        
        return response
    
    #checks if theres any SQL query errors
    try:

        #initializing path parameter for future use.
        sdId = int(event['pathParameters']['surveyId'])

        #A mySQL query that retrieves a survey definition according to the passed in id value. 
        with connection.cursor() as cursor:
            query = "SELECT * FROM SurveyDefinition WHERE id = %s;"
            cursor.execute(query, sdId)
            records = cursor.fetchone()

    except ValueError as ve:
        msg = "ERROR: A path parameter(s) does not have a proper data type."
        error = str(ve)
        response = errorResponse(msg,error,400)
        return response

    except Exception as e:
        msg = "ERROR: Invalid or incorrect mySQL query"
        error = str(e)
        response = errorResponse(msg, error, 400)
        return response

    finally:
        connection.close()

    try:
        #records will be of None type if the survey definition does not exist. 
        if records is None:
            #returns a message that the survey definition doesn't exist. 
            raise TypeError("ERROR: The survey definition id does not exist.")
        else:
            #returns pages in JSON data format
            body = {"SurveyDefinition": records}
            bodyToJson = json.dumps(body,indent=4,default=str)
            response = createResponse(bodyToJson, 200)
            return response
    except TypeError as te:
        msg = str(te)
        body = {"Message": msg}
        bodyToJson = json.dumps(body,indent=4,default=str)
        response = createResponse(bodyToJson,404)
        return response
           

#creates a connection with the database. 
def connect(host):

    connection = pymysql.connect(host = host, db = os.environ.get('DB_NAME'),
    user = os.environ.get('DB_USERNAME'), password = os.environ.get('DB_PASSWORD'),  
    cursorclass=pymysql.cursors.DictCursor)

    return connection


#creates the response JSON body
def createResponse(body, status):
    response = {
    "statusCode":status, 
    "headers":{"Access-Control-Allow-Origin": os.environ.get('ORIGIN')},
    "body":body
    }

    return response

#creates the error response JSON body
def errorResponse(msg,error,status):
    obj = {"message": msg, "error": error}
    objToJson = json.dumps(obj,indent=4,default=str)
    response = createResponse(objToJson, status)

    return response
