import pymysql
import pymysql.cursors
import os
import json
import traceback

#retrieves pages in a survey from the database 
def lambda_handler(event, context):
    host = os.environ.get('DB_HOST')
    connection = None

    #checks if connection was successful
    try:
        connection = connect(host)
    except Exception as e:

        msg = "ERROR: Failed to connect to database."
        error = str(e)
        response = errorResponse(msg, error,400)
        
        return response
    
    #checks if theres any SQL query errors
    try:

        #initializing path parameter values for future use
        sdId = int(event['pathParameters']['surveyDefinitionId'])
        
        #A mySQL query that joins SurveyDefinition, DisplayGroup, DisplayGroupPage, DisplayGroupItem,
        #and QuestionLang tables to retrieve all the pages associated with a survey definition/display group.
        with connection.cursor() as cursor:
            query = """
            SELECT dgp.id,dgp.startPage, dgn.nextPageId, ql.questionHeader, ql.questionBody
            FROM SurveyDefinition AS sd 
                INNER JOIN DisplayGroup AS dg ON sd.id = dg.surveyDefinitionId
                INNER JOIN DisplayGroupNavigation AS dgn ON dgn.displayGroupId = dg.id
                INNER JOIN DisplayGroupPage AS dgp ON dgp.displayGroupId = dg.id
                INNER JOIN DisplayGroupItem AS dgi ON dgi.pageId = dgp.id
                INNER JOIN QuestionLang AS ql ON ql.questionId = dgi.questionId
			WHERE sd.id = %s AND dgp.id = dgn.pageId 
            AND (evalPriority = 99 or evalPriority = 9) AND ql.version = dgi.version
            GROUP BY dgp.id, dgn.nextPageId
            ORDER BY dgp.startPage DESC, dgp.id;"""
            cursor.execute(query, sdId)
            records = cursor.fetchall()
            unlinkedList = list(records)

            index = 0
            temp = None
            pos = None

            #Sorts the list in the order that the pages will appear in a survey. 
            while index < len(unlinkedList):
                nextPage = unlinkedList[index]['nextPageId']
                
                if nextPage is None:
                    break

                if findPosition(unlinkedList, nextPage, index) is None:
                    raise Exception("ERROR: One of the pages does not have an item.")

                pos = findPosition(unlinkedList, nextPage, index)
                temp = unlinkedList[pos]
                unlinkedList.pop(pos)
                unlinkedList.insert(index+1,temp)
                index += 1
            #Slices the list to separate the linked survey pages and the possible branching pages. 
            branchQuestList = unlinkedList[index+1:len(unlinkedList)]
            linkedList = unlinkedList[:index+1]
    
    except ValueError as ve:
        msg = "ERROR: A path parameter(s) does not have a proper data type."
        error = str(ve)
        response = errorResponse(msg,error,400)
        connection.rollback()
        return response

    except Exception as e:
        msg = "ERROR: Invalid values and/or incorrect mySQL queries."
        error = str(e)
        response = errorResponse(msg, error, 400)
        connection.rollback()
        return response
    
    finally:
        connection.close()
    
    try:
        #records will have a length of 0 if the surveyDefinitionId doesn't exist, or they are not related to one another. 
        if len(records) == 0:
            #returns a message that there are no pages associated with the two id's
            raise TypeError("ERROR: The survey definition id does not exist. ")
        else:
            #returns pages in JSON data format
            body = {"DisplayGroupPages": linkedList, "BranchingQuestions": branchQuestList}
            bodyToJson = json.dumps(body,indent=4,default=str)
            response = createResponse(bodyToJson,200)
            return response
    except TypeError as te:
        msg = str(te)
        body = {"Message": msg}
        bodyToJson = json.dumps(body,indent=4,default=str)
        response = createResponse(bodyToJson,404)
        return response


#creates a connection with the database. 
def connect(host):

    connection = pymysql.connect(host = host, db = os.environ.get('DB_NAME'),
    user = os.environ.get('DB_USERNAME'), password = os.environ.get('DB_PASSWORD'),  
    cursorclass=pymysql.cursors.DictCursor)

    return connection


#creates the response JSON body
def createResponse(body, status):
    response = {
    "statusCode":status, 
    "headers":{"Access-Control-Allow-Origin": os.environ.get('ORIGIN')},
    "body":body
    }

    return response

#creates error response JSON body
def errorResponse(msg,error,status):
    obj = {"message": msg, "error": error}
    objToJson = json.dumps(obj,indent=4,default=str)
    response = createResponse(objToJson, status)

    return response

#Helper function that finds where the next page of a pageId is located in the list. 
def findPosition(pageList, pageId, currIndex):
    #Since the list is sorted, the list can be split in half 
    #and be searched more quickly. This will depend if the pageId 
    #passed in will be equal, less or greater than the middle page. 
    middleIndex = int((len(pageList)+currIndex)/2)
    middlePage = int(pageList[middleIndex]['id'])

    if pageId == middlePage:
        return middleIndex

    elif pageId < middlePage:
        for index in range(currIndex, middleIndex):
            if pageList[index]['id'] == pageId:
                return index
    
    else:
        for index in range(middleIndex, len(pageList)):
            if pageList[index]['id'] == pageId:
                return index
        
            










