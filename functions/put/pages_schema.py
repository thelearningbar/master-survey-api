from schema import Schema

class PagesSchema:

    #creating a Schema object to make sure that the JSON input
    #matches the keys and value types of this object. 
    schema = Schema({
        'createdByUserName': str,
        'DisplayGroupNavigation': {
            'nextPageId': int,
        }
    })

    @classmethod
    def validate(self, data: dict):
        return self.schema.validate(data)

    @classmethod
    def is_valid(self, data: dict):
        return self.schema.is_valid(data)