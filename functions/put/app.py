import pymysql
import pymysql.cursors
import os
import json
from pages_schema import PagesSchema
from schema import SchemaError


def lambda_handler(event, context):
    host = os.environ.get('DB_HOST')
    connection = None

    #checks if connection was successful
    try:
        connection = connect(host)
    except Exception as e:
        msg = "Failed to connect to database."
        error = str(e)
        response = errorResponse(msg, error,400)
        return response
    
     #validates the input
    try:
        input = json.loads(event['body'])
        validate = PagesSchema.validate(input)
    except SchemaError as se:
        message = "Input is not in the form of JSON."
        error = str(se)
        response = errorResponse(message, error, 400)
        return response
    
    #checks if theres any SQL query errors
    try:
        pageId = int(event['pathParameters']['pageId'])
        created_by_username = validate['createdByUserName']
        displayGroupNav = validate['DisplayGroupNavigation']
        
        #Checks if nextPageId equals the pageId to be moved.
        if pageId == displayGroupNav['nextPageId']:
            raise Exception("ERROR: pageId to be moved cannot be the previous or next page.")
        

        #Check if the nextPageId exists in the display group the pageId belongs to. 
        with connection.cursor() as cursor:
            #retrieves displayGroupId
            query = "SELECT displayGroupId FROM DisplayGroupNavigation where pageId = %s;"
            cursor.execute(query, pageId)
            record = cursor.fetchone()
            dgId = record['displayGroupId']

            #retrieves previousPageId(s) pointing to new nextPageId. 
            query = "SELECT DISTINCT pageId, evalPriority FROM DisplayGroupNavigation where nextPageId = %s;"
            cursor.execute(query, displayGroupNav['nextPageId'])
            record = cursor.fetchall()
            prevPageList = record
            
            #If the nextPageId doesn't exists, there should be no records returned.
            query = "SELECT DISTINCT pageId FROM DisplayGroupNavigation WHERE pageId = %s AND displayGroupId = %s;"
            cursor.execute(query, (displayGroupNav['nextPageId'], dgId))
            record = cursor.fetchall()

            if len(record) == 0:
                raise TypeError("ERROR: nextPageId and/or previousPageId does not exist in this display group.")

        #retrieve current previousPageId(s) of to-be-moved page.
        with connection.cursor() as cursor:
            query = "SELECT DISTINCT pageId, evalPriority FROM DisplayGroupNavigation WHERE nextPageId = %s;"
            cursor.execute(query, pageId)
            record = cursor.fetchall()
            currPrevPageList = record
        
        #retrieve current nextPageId of to-be-moved page.
        with connection.cursor() as cursor:
            query = "SELECT DISTINCT nextPageId FROM DisplayGroupNavigation WHERE pageId = %s AND evalPriority = 99;"
            cursor.execute(query, pageId)
            record = cursor.fetchone()
            currNextPageId = record['nextPageId']

            if displayGroupNav['nextPageId'] == currNextPageId:
                raise Exception("Cannot be moved to the same place.")

        #Checks if the page to be moves branches, or is a single page. 
        with connection.cursor() as cursor:
            #a query that will check if the pageId has multiple nextPageIds. 
            query = "SELECT DISTINCT nextPageId FROM DisplayGroupNavigation WHERE pageId = %s;"
            cursor.execute(query, pageId)
            checkList = cursor.fetchall()
            
            index = 0
            counter = 0
            temp = None
            nextPageList = []
            evalList = []

            if len(checkList) > 1:
                branch = True
                #The purpose of this loop is to find out the last pageId(s) that points to the currentNextPageId. 
                #The pageId's will then be saved in a list for recconnection later. 
                #Saving the evalPriority is also important since the same pageId may point to different pages. 
                while index < len(checkList):
                    if checkList[index]['nextPageId'] == currNextPageId:
                        query = "SELECT DISTINCT evalPriority FROM DisplayGroupNavigation WHERE pageId = %s and nextPageId = %s;"
                        cursor.execute(query, (pageId, currNextPageId))
                        eval = cursor.fetchall()
                        evalList.append(eval)
                        nextPageList.append(pageId)
                        index += 1

                    else:
                        #Grab the nextPageId of the current page in the checkList. Keep doing this until 
                        #the pageId that connects to the currNextPageId is found. 
                        query = "SELECT nextPageId FROM DisplayGroupNavigation WHERE pageId = %s;"
                        cursor.execute(query, checkList[index]['nextPageId'])
                        pageTemp = cursor.fetchall()
                        page = None

                        #This if else block is to handle when 99 and 9999 have different nextPageIds. 
                        if len(pageTemp) > 1:
                            if pageTemp[1]['nextPageId'] == currNextPageId:
                                page = pageTemp[1]
                            else:
                                page = pageTemp[0]
                        else:
                            page = pageTemp[0]
                        
                        #If the nextPageId is equal to the currNextPageId, make sure to add the pageId associated to
                        #that nextPageId in the list to be reconnected later. 
                        if page['nextPageId'] == currNextPageId:
                            nextPageList.append(checkList[index]['nextPageId'])

                            #This query is used to grab the correct evalPriorities so that the correct pageId's are changed. 
                            query = "SELECT DISTINCT evalPriority FROM DisplayGroupNavigation WHERE pageId = %s and nextPageId = %s;"
                            cursor.execute(query, (checkList[index]['nextPageId'], page['nextPageId']))
                            eval = cursor.fetchall()
                            evalList.append(eval)
                            index += 1

                        #If it doesn't already equal the currentNextPageId, find the nextPageId of it.
                        #Keep going in case one of the pages point to the currNextPageId. 
                        #Hard coded to only go 5 iterations, theres no way of telling how long a branch will be.
                        elif page['nextPageId'] != currNextPageId:
                            temp = page['nextPageId']
                            counter = 0
                            page2 = None
                            while counter < 5:
                                query = "SELECT nextPageId FROM DisplayGroupNavigation WHERE pageId = %s;"
                                cursor.execute(query, temp)
                                page2Temp = cursor.fetchall()

                                if len(page2Temp) > 1:
                                    if page2Temp[1]['nextPageId'] == currNextPageId:
                                        page2 = page2Temp[1]
                                    else:
                                        page2 = page2Temp[0]
                                else:
                                    page2 = page2Temp[0]

                                if page2['nextPageId'] == currNextPageId:
                                    nextPageList.append(temp)
                                    query = "SELECT DISTINCT evalPriority FROM DisplayGroupNavigation WHERE pageId = %s and nextPageId = %s;"
                                    cursor.execute(query, (temp, page2['nextPageId']))
                                    eval = cursor.fetchall()
                                    evalList.append(eval)
                                    index += 1
                                    counter = 5
                                    temp = None

                                else:
                                    temp = page2['nextPageId']
                                    counter += 1

                        else:
                            index += 1
            else:
                branch = False

        #connect adjacent pages where it moved from.  
        with connection.cursor() as cursor:
            for page in currPrevPageList:
                query = """UPDATE DisplayGroupNavigation SET nextPageId = %s, lastModifiedByUserName = %s, lastModifiedDate = now() 
                WHERE pageId = %s AND evalPriority = %s;"""
                cursor.execute(query, (currNextPageId,created_by_username, page['pageId'], page['evalPriority']))

        #connect to-be-moved page to its new next page. 
        with connection.cursor() as cursor:
            #zip() will map a pageId to its corresponding evalPriority that needs to be changed and create a tuple.
            #list() will turn it into a list. 
            newList = list(zip(nextPageList,evalList))
            print(newList)
            if branch:
                for tuple in newList:
                    for eval in tuple[1]:
                        #This if statement makes sure that records with evalPriority of 99 and 9999 are changed 
                        #and have the same nextPageIds. 
                        if int(eval['evalPriority']) == 99 or int(eval['evalPriority']) == 9999:
                            query = """UPDATE DisplayGroupNavigation SET nextPageId = %s, lastModifiedByUserName = %s, lastModifiedDate = now() 
                            WHERE pageId = %s and (evalPriority = %s or evalPriority = %s);"""
                            cursor.execute(query, (displayGroupNav['nextPageId'],created_by_username, tuple[0], 99, 9999))
                        else:
                            query = """UPDATE DisplayGroupNavigation SET nextPageId = %s, lastModifiedByUserName = %s, lastModifiedDate = now() 
                            WHERE pageId = %s and evalPriority = %s;"""
                            cursor.execute(query, (displayGroupNav['nextPageId'],created_by_username, tuple[0], eval['evalPriority']))
            else:
                query = """UPDATE DisplayGroupNavigation SET nextPageId = %s, lastModifiedByUserName = %s, lastModifiedDate = now() 
                WHERE pageId = %s;"""
                cursor.execute(query, (displayGroupNav['nextPageId'],created_by_username, pageId))

        #connect previous page to to-be-moved page. 
        with connection.cursor() as cursor:
            for page in prevPageList:
                query = """UPDATE DisplayGroupNavigation SET nextPageId = %s, lastModifiedByUserName = %s, lastModifiedDate = now() 
                WHERE pageId = %s AND evalPriority = %s;"""
                cursor.execute(query, (pageId,created_by_username, page['pageId'], page['evalPriority']))


    except ValueError as ve:
        msg = "A path parameter(s) does not have a proper data type."
        error = str(ve)
        response = errorResponse(msg,error,400)
        connection.rollback()
        return response
    
    except TypeError as te:
        msg = "One of the id's does not exist."
        error = str(te)
        response = errorResponse(msg, error, 404)
        connection.rollback()
        return response

    except Exception as e:
        msg = "Invalid values or incorrect mySQL queries."
        error = str(e)
        response = errorResponse(msg, error, 400)
        connection.rollback()
        return response

    finally:
        connection.commit()

    #returns pages in JSON data format
    body = {
        "Message": "Move operation completed successfully",
        "DisplayGroupNavigation":{
            "pageId": pageId,
            "branch": branch
        }
    }
    bodyToJson = json.dumps(body, indent = 4, default=str)
    response = createResponse(bodyToJson, 200)

    return response


#creates a connection with the database. 
def connect(host):

    connection = pymysql.connect(host = host, db = os.environ.get('DB_NAME'),
    user = os.environ.get('DB_USERNAME'), password = os.environ.get('DB_PASSWORD'),  
    cursorclass=pymysql.cursors.DictCursor)

    return connection


#creates the response JSON body
def createResponse(body, status):
    response = {
    "statusCode":status, 
    "headers":{"Access-Control-Allow-Origin": os.environ.get('ORIGIN')},
    "body":body
    }

    return response

#creates error response JSON body
def errorResponse(msg,error,status):
    obj = {"message": msg, "error": error}
    objToJson = json.dumps(obj,indent=4,default=str)
    response = createResponse(objToJson, status)

    return response


    

