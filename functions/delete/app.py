import pymysql
import pymysql.cursors
import os
import json


def lambda_handler(event, context):
    host = os.environ.get('DB_HOST')
    connection = None

    #checks if connection was successful to database
    try:
        connection = connect(host)
    except Exception as e:
        msg = "Failed to connect to database."
        error = str(e)
        response = errorResponse(msg, error,400)
        return response
    
    #checks if theres any SQL query errors
    try:
        pageId = int(event['pathParameters']['pageId'])

        with connection.cursor() as cursor:
            query = "SELECT id FROM DisplayGroupPage WHERE id = %s;"
            cursor.execute(query, pageId)
            check = cursor.fetchone()

            query = "SELECT pageId FROM DisplayGroupNavigation WHERE pageId = %s;"
            cursor.execute(query, pageId)
            check2 = cursor.fetchone()

            if check is None:
                raise TypeError("pageId does not exist in DisplayGroupPage.")

            if check2 is None:
                skip = True

        with connection.cursor() as cursor:
            if skip is False:
                #retrieve next page to reconnect for later
                query = "SELECT nextPageId FROM DisplayGroupNavigation WHERE pageId = %s;"
                cursor.execute(query, pageId)
                records = cursor.fetchone()
                nextPageId = records['nextPageId']

                #retrieve previous page to connect to next page
                query = "SELECT pageId FROM DisplayGroupNavigation WHERE nextPageId = %s;"
                cursor.execute(query, pageId)
                records = cursor.fetchone()
                prevPageId = records['pageId']

        #deletes a page from a survey, including records that reference (foreign keys) id of DisplayGroupPage.
        #These tables include DisplayGroupNavigation, DisplayGroupItem, MeasureDisplayGroupPage and SurveyFlowTrail table. 
        with connection.cursor() as cursor:

            query = "DELETE FROM DisplayGroupNavigation WHERE pageId = %s;"
            cursor.execute(query, pageId)

            query = "DELETE FROM DisplayGroupItem WHERE pageId = %s;"
            cursor.execute(query, pageId)

            query = "DELETE FROM MeasureDisplayGroupPage WHERE displayGroupPageId = %s;"
            cursor.execute(query, pageId)

            query = "DELETE FROM SurveyFlowTrail WHERE pageId = %s;"
            cursor.execute(query, pageId)

            query = "DELETE FROM SurveyFlowTrail WHERE prevPageId = %s;"
            cursor.execute(query, pageId)

            query = "DELETE FROM DisplayGroupPage WHERE id = %s;"
            cursor.execute(query, pageId)
            
        
        #connect the previous page to the next page
        with connection.cursor() as cursor:
            if skip is False:
                query = "UPDATE DisplayGroupNavigation SET nextPageId = %s, lastModifiedDate = now() WHERE pageId = %s;"
                cursor.execute(query, (nextPageId,prevPageId))
        
    except ValueError as ve:
        msg = "ERROR: A parameter(s) does not have a proper data type."
        error = str(ve)
        response = errorResponse(msg, error, 400)
        connection.rollback()
        return response

    except TypeError as te:
        msg = "ERROR: pageId does not exist."
        error = str(te)
        response = errorResponse(msg, error, 404)
        connection.rollback()
        return response

    except Exception as e:
        msg = "ERROR: Invalid values and/or incorrect mySQL queries"
        error = str(e)
        response = errorResponse(msg, error, 400)
        connection.rollback()
        return response

    finally:
        connection.commit()

    #returns pages in JSON data format
    body = {
        "Message": "Delete operation completed successfully"
    }
    bodyToJson = json.dumps(body, indent = 4, default=str)
    response = createResponse(bodyToJson, 200)
    return response


#creates a connection with the database. 
def connect(host):

    connection = pymysql.connect(host = host, db = os.environ.get('DB_NAME'),
    user = os.environ.get('DB_USERNAME'), password = os.environ.get('DB_PASSWORD'),  
    cursorclass=pymysql.cursors.DictCursor)

    return connection


#creates the response JSON body
def createResponse(body, status):
    response = {
    "statusCode":status, 
    "headers":{"Access-Control-Allow-Origin": os.environ.get('ORIGIN')},
    "body":body
    }

    return response

#creates error response JSON body
def errorResponse(msg,error,status):
    obj = {"message": msg, "error": error}
    objToJson = json.dumps(obj,indent=4,default=str)
    response = createResponse(objToJson, status)

    return response