import pymysql
import pymysql.cursors
import os
import json
from copy_schema import CopySchema
from schema import SchemaError

def lambda_handler(event, context):
    host = os.environ.get('DB_HOST')
    connection = None

    #checks if connection was successful
    try:
        connection = connect(host)
    except Exception as e:
        msg = "Failed to connect to database."
        error = str(e)
        response = errorResponse(msg, error, 502)
        return response

    #validates the input from the request body. 
    try:
        input = json.loads(event['body'])
        validate = CopySchema.validate(input)
    except SchemaError as se:
        message = "Input is not in the form of JSON."
        error = str(se)
        response = errorResponse(message, error, 400)
        return response

    try:
        pageId = event['pathParameters']['pageId']
        newDisplayGroupId = event['pathParameters']['displayGroupId']
        created_by_username = validate['createdByUserName']

        #retrieve DisplayGroupPage object to be copied. 
        with connection.cursor() as cursor:
            query = "SELECT * FROM DisplayGroupPage WHERE id = %s;"
            cursor.execute(query, pageId)
            copyPage = cursor.fetchone()
        
        #retrieve DisplayGroupItem objects to be copied. 
        with connection.cursor() as cursor: 
            query = "SELECT * FROM DisplayGroupItem WHERE pageId = %s;"
            cursor.execute(query, pageId)
            copyItems = cursor.fetchall()

        #copy page into new displayGroup
        with connection.cursor() as cursor: 
            query = """INSERT INTO DisplayGroupPage (displayGroupId, startPage, progressBarPageNumber, displayInProgressBar, minAnswerNumber, 
            maxAnswerSum, active, displayTimeBar, image, pageIntro, pageIntroTag, pageHeaderId,createdByUserName, 
            createdDate, lastModifiedByUserName, lastModifiedDate) VALUES (%s, %s, %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,now(),%s,%s);"""
            cursor.execute(query, (newDisplayGroupId, copyPage['startPage'], copyPage['progressBarPageNumber'], 
            copyPage['displayInProgressBar'],copyPage['minAnswerNumber'], copyPage['maxAnswerSum'], 
            copyPage['active'], copyPage['displayTimeBar'], copyPage['image'], copyPage['pageIntro'], 
            copyPage['pageIntroTag'], copyPage['pageHeaderId'], created_by_username, copyPage['lastModifiedByUserName'], 
            copyPage['lastModifiedDate']))

        #copy items into new copied page and displayGroup.    
        with connection.cursor() as cursor:
            query = "SELECT id FROM DisplayGroupPage ORDER BY id DESC LIMIT 1;"
            cursor.execute(query)
            record = cursor.fetchone()
            newPageId = record['id']

            for item in copyItems:
                query = """INSERT INTO DisplayGroupItem (pageId, questionId, version, ordering, displayQuestionScale, displayQuestionHeader,
                displayVertically, displayStyleId, isFollowUp, toggleCondition, required, subsetNumber, 
                totalSubsets, createdByUserName, createdDate, lastModifiedByUserName, lastModifiedDate) 
                VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,now(),%s,%s);"""
                cursor.execute(query, (newPageId, item['questionId'], item['version'],item['ordering'],item['displayQuestionScale'],
                item['displayQuestionHeader'],item['displayVertically'], item['displayStyleId'], item['isFollowUp'], item['toggleCondition'], 
                item['required'],item['subsetNumber'], item['totalSubsets'], created_by_username, item['lastModifiedByUserName'], 
                item['lastModifiedDate']))

        #creates MeasureDisplayGroupPage records. Must be done if a page is being added to the master survey. 
        with connection.cursor() as cursor:
            for item in copyItems:
                query = """select DISTINCT MeasureQuestion.measureId from DisplayGroupItem 
                INNER JOIN DisplayGroupPage ON DisplayGroupItem.pageId = DisplayGroupPage.id
                INNER JOIN MeasureQuestion ON MeasureQuestion.questionId = DisplayGroupItem.questionId 
                WHERE DisplayGroupPage.id = %s;"""
                cursor.execute(query, pageId)
                measures = cursor.fetchall()

            for m in measures:
                query = """INSERT INTO MeasureDisplayGroupPage (measureId, displayGroupPageId, createdByUserName, 
                createdDate, lastModifiedByUserName, lastModifiedDate) VALUES (%s, %s, %s, now(), null, null);"""
                cursor.execute(query, (m['measureId'], newPageId,created_by_username))

    except Exception as e:
        error = str(e)
        msg = "Error: Invalid values and/or incorrect mySQL queries."
        response = errorResponse(error, msg, 400)
        connection.rollback()
        return response

    finally:
        connection.commit()


    #returns pages in JSON data format
    body = {
        "Message": "Copy operation completed successfully",
        "misc": str(measures)
    }
    bodyToJson = json.dumps(body, indent=4, default=str)
    response = createResponse(bodyToJson, 201)
    return response


#creates a connection with the database. 
def connect(host):

    connection = pymysql.connect(host = host, db = os.environ.get('DB_NAME'),
    user = os.environ.get('DB_USERNAME'), password = os.environ.get('DB_PASSWORD'),  
    cursorclass=pymysql.cursors.DictCursor)

    return connection


#creates the response JSON body
def createResponse(body, status):
    response = {
    "statusCode":status, 
    "headers":{"Access-Control-Allow-Origin": os.environ.get('ORIGIN')},
    "body":body
    }

    return response

#creates error response JSON body
def errorResponse(msg,error,status):
    obj = {"message": msg, "error": error}
    objToJson = json.dumps(obj,indent=4,default=str)
    response = createResponse(objToJson, status)

    return response