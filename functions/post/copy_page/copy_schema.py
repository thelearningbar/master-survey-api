from schema import Schema

class CopySchema:

    #creating a Schema object to make sure that the JSON input
    #matches the Schema object's keys and value types. 
    schema = Schema({'createdByUserName': str})

    @classmethod
    def validate(self, data: dict):
        return self.schema.validate(data)

    @classmethod
    def is_valid(self, data: dict):
        return self.schema.is_valid(data)