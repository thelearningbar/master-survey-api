from schema import Schema, Or

class PagesSchema:

    #creating a Schema object to make sure that the JSON input
    #matches the Schema object's keys and value types. 
    schema = Schema({
        'createdByUserName': str,
        'DisplayGroupNavigation': {
            'evalString': Or(str, None),
            'evalPriority': int,
            'nextPageId': Or(int, None)
        }
    })

    @classmethod
    def validate(self, data: dict):
        return self.schema.validate(data)

    @classmethod
    def is_valid(self, data: dict):
        return self.schema.is_valid(data)

    