import pymysql
import pymysql.cursors
import os
import json
from pageschema import PagesSchema
from schema import SchemaError


def lambda_handler(event, context):
    host = os.environ.get('DB_HOST')
    connection = None

    #checks if connection was successful
    try:
        connection = connect(host)
    except Exception as e:
        msg = "Failed to connect to database."
        error = str(e)
        response = errorResponse(msg, error, 502)
        return response

    #validates the input from the request body. 
    try:
        input = json.loads(event['body'])
        validate = PagesSchema.validate(input)
    except SchemaError as se:
        message = "Input is not in the form of JSON."
        error = str(se)
        response = errorResponse(message, error, 400)

        return response
    
    #checks if theres any SQL query errors
    try:
        created_by_username = validate['createdByUserName']
        displayGroupNav = validate['DisplayGroupNavigation']
        dgId = int(event['pathParameters']['displayGroupId'])
        pageId = int(event['pathParameters']['pageId'])

        if pageId == displayGroupNav['nextPageId']:
            raise Exception("ERROR: nextPageId cannot be the same as the pageId.")

        #check if the nextPageId inside the request JSON body exists in
        #the display group id the pageId belongs to. 
        with connection.cursor() as cursor:
            query = "SELECT pageId FROM DisplayGroupNavigation WHERE pageId = %s AND displayGroupId = %s;"
            cursor.execute(query, (displayGroupNav['nextPageId'], dgId))
            record = cursor.fetchone()

            try:
                if record is None:
                    raise TypeError("ERROR: nextPageId does not exist in this display group. ")
            except TypeError as te:
                error = str(te)
                msg = {
                    "message": error
                }
                bodyToJson = json.dumps(msg, indent = 4, default=str)
                response = createResponse(bodyToJson, 400)
                return response

        #must check if the pageId that is being added already exists,
        #ensuring that the to be added page is inserted correctly. 
        #if check is True, pageId does not exist yet. 
        with connection.cursor() as cursor:
            query = "SELECT evalPriority, evalString FROM DisplayGroupNavigation WHERE pageId = %s;"
            cursor.execute(query, pageId)
            record2 = cursor.fetchall()
            check = False
            if len(record2) is 0:
                check = True
            else:
                for obj in record2:
                    if obj['evalPriority'] == displayGroupNav['evalPriority'] and obj['evalString'] == displayGroupNav['evalString']:
                        raise Exception("This page already exists in the database. ")
                    else:
                        check = False
        
        #retrieves created date of page to be added
        with connection.cursor() as cursor:
            query = "SELECT createdDate FROM DisplayGroupPage WHERE id = %s;"
            cursor.execute(query, pageId)
            record = cursor.fetchone()
            created_date = record['createdDate']

        #inserts new page and connects it to next page in DisplayGroupNavigation table.
        with connection.cursor() as cursor:
            query = """INSERT INTO DisplayGroupNavigation (displayGroupId, pageId,
            evalString, evalPriority, nextPageId, createdByUserName, createdDate,lastModifiedDate) VALUES 
            (%s, %s, %s, %s, %s,%s, %s, now());"""

            cursor.execute(query, (dgId,pageId, displayGroupNav['evalString'], displayGroupNav['evalPriority'], 
            displayGroupNav['nextPageId'], created_by_username, created_date))

        #retrieves and connects the previous page to new page.
        with connection.cursor() as cursor:
            if check is True:
                query = "SELECT pageId FROM DisplayGroupNavigation WHERE nextPageId = %s;"
                cursor.execute(query, displayGroupNav['nextPageId'])
                record = cursor.fetchone()
                prevPageId = record['pageId']
            else:
                query = "SELECT pageId FROM DisplayGroupNavigation WHERE nextPageId = %s;"
                cursor.execute(query, pageId)
                record = cursor.fetchone()
                prevPageId = record['pageId']

        with connection.cursor() as cursor:
            query = "UPDATE DisplayGroupNavigation SET nextPageId = %s, lastModifiedByUserName = %s, lastModifiedDate = now() WHERE pageId = %s;"
            cursor.execute(query, (pageId, created_by_username, prevPageId))

    except ValueError as ve:
        msg = "A path parameter(s) does not have a proper data type."
        error = str(ve)
        response = errorResponse(msg,error,400)
        connection.rollback()
        return response
    
    except TypeError as te:
        msg = "Display group and/or pageId does not exist, or the pageId is not associated with specified display group."
        error = str(te)
        response = errorResponse(msg, error, 404)
        connection.rollback()
        return response

    except Exception as e:
        msg = "Invalid/Incorrect mySQL query"
        error = str(e)
        response = errorResponse(msg, error, 400)
        connection.rollback()
        return response

    finally:
        connection.commit()

    #returns pages in JSON data format
    body = {
        "Message": "Add operation completed successfully",
        "DisplayGroupNavigation":{
            "prevPageId": prevPageId,
            "pageId": pageId,
            "nextPageId": displayGroupNav['nextPageId']
        }
    }
    bodyToJson = json.dumps(body, indent = 4, default=str)
    response = createResponse(bodyToJson, 201)

    return response


#creates a connection with the database. 
def connect(host):

    connection = pymysql.connect(host = host, db = os.environ.get('DB_NAME'),
    user = os.environ.get('DB_USERNAME'), password = os.environ.get('DB_PASSWORD'),  
    cursorclass=pymysql.cursors.DictCursor)

    return connection


#creates the response JSON body
def createResponse(body, status):
    response = {
    "statusCode":status, 
    "headers":{"Access-Control-Allow-Origin": os.environ.get('ORIGIN')},
    "body":body
    }

    return response

#creates error response JSON body
def errorResponse(msg,error,status):
    obj = {"message": msg, "error": error}
    objToJson = json.dumps(obj,indent=4,default=str)
    response = createResponse(objToJson, status)

    return response

